package email

import (
	"context"
	"crypto/rand"
	"errors"
	"fmt"
	"math/big"
	"time"

	"gitlab.com/some_prodject_on_microservices/notificator/app"
	"gitlab.com/some_prodject_on_microservices/notificator/models"
)

type emailNotifycator struct {
}

func NewEmailNotifycator() app.Notificator {
	return &emailNotifycator{}
}

func (n *emailNotifycator) Notify(ctx context.Context, msg models.Notification) error {
	time.Sleep(1 * time.Second)

	num, err := rand.Int(rand.Reader, big.NewInt(10))
	if err != nil {
		return err
	}

	if num.Int64() > 1 {
		fmt.Println(fmt.Sprintf("Success send notification: '%s' to %s", msg.Text, msg.User.Email))
		return nil
	} else {
		return errors.New(fmt.Sprintf("Failed send notification: '%s' to %s", msg.Text, msg.User.Email))
	}

}
