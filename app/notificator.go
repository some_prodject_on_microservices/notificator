package app

import (
	"context"

	"gitlab.com/some_prodject_on_microservices/notificator/models"
)

type Notificator interface {
	Notify(ctx context.Context, msg models.Notification) error
}
