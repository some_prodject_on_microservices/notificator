package rabbit

import (
	"context"
	"encoding/json"

	"github.com/streadway/amqp"
	"gitlab.com/some_prodject_on_microservices/notificator/app"
	"gitlab.com/some_prodject_on_microservices/notificator/models"
)

type Handler struct {
	emailNotificator app.Notificator
}

func NewHandler(emailNotificator app.Notificator) *Handler {
	return &Handler{
		emailNotificator: emailNotificator,
	}
}

func (h *Handler) AcceptNotificationMsg(msg amqp.Delivery) {
	var (
		notification models.Notification
		notificator  app.Notificator
	)

	err := json.Unmarshal(msg.Body, &notification)
	if err != nil {
		return
	}

	switch notification.NotificationType {
	case models.Email:
		notificator = h.emailNotificator
	}

	err = notificator.Notify(context.Background(), notification)
	if err != nil {
		return
	}

	msg.Ack(false)
}
