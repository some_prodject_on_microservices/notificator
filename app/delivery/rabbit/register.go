package rabbit

import (
	"gitlab.com/some_prodject_on_microservices/notificator/app"
	queueclient "gitlab.com/some_prodject_on_microservices/notificator/pkg/QueueClient"
)

const NotifyQueueName = "notify"

func RegisterConsumers(qCli queueclient.QueueClient, notificator app.Notificator) {
	h := NewHandler(notificator)

	msgChan, err := qCli.Consume(NotifyQueueName)
	if err != nil {
		panic(err)
	}

	for msg := range msgChan {
		h.AcceptNotificationMsg(msg)
	}
}
