package models

type Notification struct {
	User User
	NotificationType
	Text string
}

type NotificationType int

const (
	Email NotificationType = iota
)
