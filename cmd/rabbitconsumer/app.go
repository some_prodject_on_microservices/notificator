package rabbitconsumer

import (
	"gitlab.com/some_prodject_on_microservices/notificator/app"
	apprabit "gitlab.com/some_prodject_on_microservices/notificator/app/delivery/rabbit"
	appnotificator "gitlab.com/some_prodject_on_microservices/notificator/app/notificator/email"
	"gitlab.com/some_prodject_on_microservices/notificator/config"
	queueclient "gitlab.com/some_prodject_on_microservices/notificator/pkg/QueueClient"
	"gitlab.com/some_prodject_on_microservices/notificator/pkg/QueueClient/rabbitmq"

	"github.com/streadway/amqp"
)

type App struct {
	rabbitConn  *amqp.Connection
	rabbitChan  *amqp.Channel
	qCli        queueclient.QueueClient
	notificator app.Notificator
}

func NewApp(conf config.Config) *App {
	con, chn, err := rabbitmq.Connect(
		conf.RabbitLogin,
		conf.RabbitPass,
		conf.RabbitHost,
		conf.RabbitPort,
	)
	if err != nil {
		panic(err)
	}
	qCli := rabbitmq.NewRabbitClient(chn)

	notif := appnotificator.NewEmailNotifycator()

	return &App{
		rabbitConn:  con,
		rabbitChan:  chn,
		qCli:        qCli,
		notificator: notif,
	}
}

func (a *App) Run() {
	apprabit.RegisterConsumers(a.qCli, a.notificator)
}

func (a *App) Stop() {
	defer a.rabbitChan.Close()
	defer a.rabbitConn.Close()
}
