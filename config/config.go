package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	Release     bool
	AppName     string
	LogDir      string
	LogFile     string
	RabbitLogin string
	RabbitPass  string
	RabbitHost  string
	RabbitPort  string
}

// InitConfig - load config from config.yml
func InitConfig() Config {
	viper.AddConfigPath("./config")
	viper.SetConfigName("config")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	conf := Config{
		Release:     viper.GetBool("release"),
		AppName:     viper.GetString("name"),
		LogDir:      viper.GetString("log.dir"),
		LogFile:     viper.GetString("log.file"),
		RabbitLogin: viper.GetString("rabbit.login"),
		RabbitPass:  viper.GetString("rabbit.pass"),
		RabbitHost:  viper.GetString("rabbit.host"),
		RabbitPort:  viper.GetString("rabbit.port"),
	}

	return conf
}
